import { Component, OnInit } from '@angular/core';
import { AppStateService } from '../../app/components/app-state.service'
import { AppDataStore } from '../../app/components/app-ds.service'
import { NavController } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';

import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {

  constructor(public appState: AppStateService, public ds: AppDataStore, public storage: Storage) {

    // this.ds.testMethod();
  }
  title = 'app';
  selectedPerson = {};
  people = [];


  changeSelectedPerson($event) {
    this.selectedPerson = $event;
  }

  loadClients() {
    console.log('trying to load clients');
    try{
    return this.ds.getClients().then((people)=>{
      this.people = people;
      console.log('people', people);
    });
    } catch(error){
      console.log('caught', error);
    }

  }

  refreshPage(refresher){
    this.loadClients().then(() => {
      refresher.complete();
    });
    
  }

  ngOnInit() {

    setTimeout(() => {
      this.loadClients();
      this.appState.setState('init');
      this.appState.setPerson({});
    }, 1000)


  }

}
