import { Component} from '@angular/core';
import { ModalController, Platform, NavParams, ViewController } from 'ionic-angular';
import { AppStateService } from '../app-state.service'


@Component({
  selector: 'app-drone-modal',
  templateUrl: './drone-modal.component.html'
})


export class DroneModalComponent {
 showPicker: boolean = false;

 constructor(
    public platform: Platform,
    public params: NavParams,
    public viewCtrl: ViewController,
    public appState: AppStateService
  ) {
  }

    // Initialized to specific date (09.10.2018).
    private scheduledFlight: Object = { date: '2018-10-11', time: '08:00' };

  next() {
    this.showPicker = true;
  }

  submit() {
    this.appState.setState('waiting');
    this.viewCtrl.dismiss();
  }

  beforeDismiss(): boolean {
    return true;
  }

  beforeClose(): boolean {
    return true;
  }
}
