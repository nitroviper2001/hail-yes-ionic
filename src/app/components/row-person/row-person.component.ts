import { Component, OnInit, Input, Output, EventEmitter, ViewContainerRef } from '@angular/core';
import { Overlay, overlayConfigFactory } from 'angular2-modal';
import { Modal, BSModalContext } from 'angular2-modal/plugins/bootstrap';
import { AppStateService } from '../app-state.service'
import { Subscription } from 'rxjs/Subscription';
import { ModalController } from 'ionic-angular';
import { DroneModalComponent } from '../drone-modal/drone-modal.component';
import {NavController} from 'ionic-angular';
import { ReviewPage } from  '../../../pages/review/review';


@Component({
  selector: 'app-row-person',
  templateUrl: './row-person.component.html'
})
export class RowPersonComponent implements OnInit {
  @Input() person: object;
  subscriptionToAppState: Subscription;
  subscriptionToPerson: Subscription;

  currentAppState: string;
  selectedPerson: object;

  // @Output()
  // selected: EventEmitter<object> = new EventEmitter<object>();

  // select() {
  //   this.selected.emit(this.person);
  // }
  constructor(public modalCtrl: ModalController, public appState: AppStateService, public navCtrl: NavController) {
    this.subscriptionToAppState = this.appState.getState().subscribe(message => {
      console.log('app state from person row', message);
      this.currentAppState = message;
    });
    this.subscriptionToPerson = this.appState.getPerson().subscribe(message => {
      this.selectedPerson = message;
    });
  }

  requestDroneFlight() {
    this.appState.setPerson(this.person);
    let modal = this.modalCtrl.create(DroneModalComponent);
    modal.present();
  }

  approve() {
    this.appState.setState('scheduled');
  }

  fly() {
    this.appState.setState('done');
  }

  review() {
    this.navCtrl.push(ReviewPage);
  }


  ngOnInit() {
    // console.log(this.person);
  }
}
