import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

import { DialogRef, ModalComponent, CloseGuard } from 'angular2-modal';
import { BSModalContext } from 'angular2-modal/plugins/bootstrap';
import {IMyDpOptions} from 'mydatepicker';

export class ScheduleModalContext extends BSModalContext {
}
  
@Component({
  selector: 'app-schedule-modal',
  templateUrl: './schedule-modal.component.html'
})


export class ScheduleModalComponent implements CloseGuard, ModalComponent<ScheduleModalContext> {
 context: ScheduleModalContext;

  private myDatePickerOptions: IMyDpOptions = {
        // other options...
        dateFormat: 'dd.mm.yyyy',
    };

    // Initialized to specific date (09.10.2018).
    private model: Object = { date: { year: 2018, month: 10, day: 9 } };

  constructor(public dialog: DialogRef<ScheduleModalContext>) {
    this.context = dialog.context;
  }

  close() {
    this.dialog.close();
  }


  beforeDismiss(): boolean {
    return true;
  }

  beforeClose(): boolean {
    return true;
  }
}
