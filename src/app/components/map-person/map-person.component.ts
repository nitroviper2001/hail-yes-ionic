import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
@Component({
  selector: 'app-map-person',
  templateUrl: './map-person.component.html'
})
export class MapPersonComponent implements OnInit {
  @Input() person: object;

  @Output()
  selected: EventEmitter<object> = new EventEmitter<object>();

  select() {
    this.selected.emit(this.person);
  }

  constructor(){}
  

  ngOnInit() {
    console.log(this.person);
  }

}
