import { Injectable } from '@angular/core';
import { DataStore, Mapper } from 'js-data';
import { HttpAdapter } from 'js-data-http';
import { LocalStorageAdapter } from 'js-data-localstorage';
import LocalForage from 'localforage';
import CordovaSQLiteDriver from 'localforage-cordovasqlitedriver';

// data provided by single GET mock at https://demo8520761.mockable.io/clients
//  people = [
//       {
//         "id": 1,
//         "name": "Andy Jensen",
//         "policy": "HO3",
//         "severity": "High",
//         "locationX": "20%",
//         "locationY": "63%"
//       },
//       {
//         "id": 2,
//         "name": "Cindy Denada",
//         "policy": "HO3",
//         "severity": "High",
//         "locationX": "60%",
//         "locationY": "80%"
//       },
//       {
//         "id": 3,
//         "name": "Jorge Gonzales",
//         "policy": "HO3",
//         "severity": "Low",
//         "locationX": "50%",
//         "locationY": "10%"
//       }
//     ]

@Injectable()
export class AppDataStore {
  public ds: DataStore;
  public http: HttpAdapter;
  // LocalStorage adapter is configured to use localforage, which uses sqlLite, indexdb, and localstorage depending
  public local: LocalStorageAdapter;
  public online: boolean = navigator.onLine;

  public async getClients() {
    // Clear in-memory cache so we're either fetching fresh from http or local
    this.ds.clear();
    if (this.online) {
      return this.ds.findAll('clients', {}, { adapter: 'http' });
    } else {
      return this.ds.findAll('clients', {}, { adapter: 'local' });
    }
  }

  constructor() {
    this.createOnlineListener();
    this.initializeDataStore();
  }

  private createOnlineListener() {
    window.addEventListener('online', () => { this.online = true; });
    window.addEventListener('offline', () => { this.online = false; });
  }

  private async initializeDataStore() {

    //Initialize JS-Data and tell it that we're going to store some thing called clients in it
    this.ds = new DataStore();
    this.ds.defineMapper('clients');

    this.registerAdapters();
    this.configureSyncToLocal();
  }

// Tell JS-Data that we're going to be talking to HTTP APIs and Localforage
  private async registerAdapters() {
    this.local = await createLocalForageConnection();
    this.http = createHttpConnection();
    this.ds.registerAdapter('http', this.http, { "default": true });
    this.ds.registerAdapter('local', this.local);

    // Create a connection to localforage using the cordova SQLite driver
    async function createLocalForageConnection() {
      await LocalForage.defineDriver(CordovaSQLiteDriver);
      let localForageConnection = LocalForage.createInstance({
        name: '_ionicstorage',
        storeName: '_ionickv'
      });
      return new LocalStorageAdapter({ storage: localForageConnection });
    }

    // Create an HTTP connection to our local node server, which will be serving our APIs
    function createHttpConnection(): HttpAdapter {
      const options = {
        basePath: 'http://demo8520761.mockable.io'
      };
      return new HttpAdapter(options);
    };
  }

  private configureSyncToLocal() {
    this.ds.on('add', (mapperName, record) => {
      let mapper: Mapper = this.ds.getMapper(mapperName);
      copyRecordsToLocal(record, mapper, this.local);
    });

    function copyRecordsToLocal(record, mapper, localAdapter) {

      if (record instanceof Array) {
        record.forEach(recordInArray => {
          copyRecordToLocalIfNotAlreadyThere(mapper, recordInArray, localAdapter);
        });
      } else {
        copyRecordToLocalIfNotAlreadyThere(mapper, record, localAdapter);
      }

      function copyRecordToLocalIfNotAlreadyThere(mapper, record, localAdapter) {
        localAdapter.find(mapper, record.id).then(client => {
          console.log('found', client);
          if (!client) {
            localAdapter.create(mapper, record)
            console.log(`created ${mapper.name} in local`, record);
          }
        })
      }
    }
  }
}
