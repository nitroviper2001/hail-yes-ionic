import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AppStateService {

  private stateSubject = new Subject<string>();
  private personSubject = new Subject<object>();

  getState(){
    return this.stateSubject.asObservable();
  }

  setState(state){
    this.stateSubject.next(state);
  }

  getPerson(){
    return this.personSubject.asObservable();
  }

  setPerson(person){
    this.personSubject.next(person);
  }

}
