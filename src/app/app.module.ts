import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { FormsModule }   from '@angular/forms';
import { MapPersonComponent } from './components/map-person/map-person.component';
import { RowPersonComponent } from './components/row-person/row-person.component';
import { DroneModalComponent } from './components/drone-modal/drone-modal.component';
import { AppStateService } from './components/app-state.service'
import { AppDataStore } from './components/app-ds.service'
import { ModalModule } from 'angular2-modal';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ReviewPage } from '../pages/review/review';

import { IonicStorageModule } from '@ionic/storage';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ReviewPage,
    MapPersonComponent,
    RowPersonComponent,
    DroneModalComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    ModalModule.forRoot(),
    FormsModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ReviewPage,
    DroneModalComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AppStateService,
    AppDataStore
  ]
})
export class AppModule {}
